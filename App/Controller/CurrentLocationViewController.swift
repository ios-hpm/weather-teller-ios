import UIKit
import Alamofire
import CoreLocation

class CurrentLocationViewController: UIViewController, CLLocationManagerDelegate {

    //MARK: Properties
    @IBOutlet weak var locationLabel: UILabel!
    
    @IBOutlet weak var weatherImageView: UIImageView!
    @IBOutlet weak var temperatureLabel: UILabel!
    
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var humidityLabel: UILabel!
    @IBOutlet weak var windSpeedLabel: UILabel!
    
    let locationManager = CLLocationManager()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .orange
        locationManager.requestWhenInUseAuthorization()
        if CLLocationManager.locationServicesEnabled() {
            locationManager.delegate = self
            locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
            locationManager.startUpdatingLocation()
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        if let location = locations.first {
            getWeatherDataBy(location)
        }
    }
    
    fileprivate func getWeatherDataBy(_ location : CLLocation){
        let lon = location.coordinate.longitude
        let lat = location.coordinate.latitude
        let URL = "https://api.openweathermap.org/data/2.5/weather?lat=\(lat)&lon=\(lon)&appid=e72ca729af228beabd5d20e3b7749713"
        
        Alamofire.request(
            URL,
            method: .get)
            .validate()
            .responseJSON{
                response in
                switch response.result{
                    
                case .success(let value) :
                    guard let weatherData = self.getWeatherInfoFrom(JSON: value) else {
                        print("nil data")
                        return}
                    self.updateUIData(with: weatherData)
                case .failure(let error):
                    print("big error \(error)")
                }
        }
    }
    
    private func printMessage(){
        print("Hey commit")
    }
    
    private func getWeatherInfoFrom(JSON : Any) -> Weather? {

        guard let dictionary = JSON as? [String: Any] else {return nil}

        guard let name = dictionary["name"] as? String else {return nil}

        guard let coord = dictionary["coord"] as? [String: Any] else {return nil}
        guard let lat = coord["lat"] as? Double else{return nil}
        guard let lon = coord["lon"] as? Double else{return nil}

        guard let weather = dictionary["weather"] as?[Any] else {return nil}
        guard let weatherData = weather[0] as? [String: Any] else {return nil}
        guard let weatherDescription = weatherData["description"] as? String else {return nil}

        guard let mainWeather = weatherData["main"] as?String else {return nil}
        
        guard let iconData = weatherData["icon"] as?String else {return nil}
        
        guard let mainData = dictionary["main"] as?[String: Any] else {return nil}
        guard let temp = mainData["temp"] as? Double else {return nil}
        
        guard let humidity = mainData["humidity"] as? Double else {return nil}
        
        guard let  windData = dictionary["wind"] as?[String: Any] else{return nil}
        guard let windSpeed = windData["speed"] as? Double else {return nil}
        
        guard let code = dictionary["cod"] as?Int else{return nil}
        let currentWeather = Weather(locationName: name, lon: lon, lat: lat, description: weatherDescription,
                                         mainWeather: mainWeather, iconString: iconData ,
                                         temp: temp, humidity: humidity, windSpeed: windSpeed,code: code)
        return currentWeather
    }
    
    private func updateUIData(with weather: Weather){
        locationLabel.text = weather.locationName
        
        let celsiusTemp = weather.temperature - 273.15
        temperatureLabel.text = String(format:"%.2f",celsiusTemp) + "°C"
        
        descriptionLabel.text = weather.weatherDescription
        
        humidityLabel.text = "Humidity: " + String(weather.humidity)
        
        windSpeedLabel.text = "Wind speed: " + String(weather.windSpeed)
        
        if UIImage(named : weather.iconString) != nil {
            weatherImageView.image = UIImage(named : weather.iconString)
        }else{
            weatherImageView.image = UIImage(named : "defaultPhoto")
        }
        
        //check for background image
        let lastChar = weather.iconString.last
        if lastChar == "d" {
            //day
            //self.view.backgroundColor = UIColor(patternImage: UIImage(named: "day_img")! )
            self.view.backgroundColor = UIColor(red: 1.0, green: 0.6, blue: 0.2, alpha: 1.00)
        }else {
            // night
            self.view.backgroundColor = UIColor(patternImage: UIImage(named: "night")!)

        }
        
    }
    
    
    @IBAction func unwindToCurrentVC(segue:UIStoryboardSegue) {
        let vc = segue.source as! NewLocationViewController
        locationLabel.text = vc.locationName.text
        getWeatherDataBy(string : locationLabel.text ?? "Moscow") { weather in
            weather.printData()
        }
    }
    
    private func getWeatherDataBy(string locationName: String, completion: @escaping (Weather) -> Void) {
        let URL = "https://api.openweathermap.org/data/2.5/weather?q=\(locationName)&appid=e72ca729af228beabd5d20e3b7749713"
        
        Alamofire.request(
            URL,
            method: .get)
            .validate()
            .responseJSON{
                response in
                switch response.result{
                    
                case .success(let value) :
                    guard let weatherData = self.getWeatherInfoFrom(JSON: value) else {
                        print("nil data")
                        return}
                    completion(weatherData)
                    self.updateUIData(with: weatherData)
                case .failure(let error):
                    print("big error \(error)")
                }
        }
    }
}

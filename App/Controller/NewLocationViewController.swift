
import UIKit

class NewLocationViewController: UIViewController {

    //MARK: Properties
    @IBOutlet weak var infoLabel: UILabel!
    @IBOutlet weak var locationName: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
}
